#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2023 Franco Masotti
#
# SPDX-License-Identifier: MIT

import copy
import io
import random
import string
import sys
import urllib.request

from bs4 import BeautifulSoup

ALPHABET: list[str] = [l for l in ''.join([string.ascii_lowercase, string.digits, '\n', 'àèéòùç@', "'", ' ', '-+#/`\":'])]


def normalize_text(text: str) -> str:
    r"""Remove duplicate whitespaces and alien characters."""
    final_text: list = list()

    i: int = 0
    while i < len(text):
        j: int = i
        while j < len(text) and text[j] in string.whitespace:
            j += 1

        if j < len(text) and j > i:
            final_text.append(' ')

        # Skip sequential whitespaces.
        i = j

        if j < len(text):
            if text[i].lower() in ALPHABET:
                final_text.append(text[i].lower())

        i += 1

    return ''.join(final_text)


def classifier(text: str, order: int = 1) -> dict[str]:
    r"""Classify sequences of letters."""
    if order < 1:
        raise ValueError

    freq: dict[str] = dict()

    offset: int = 0
    done: bool = False

    while not done:
        chunk: str = text[offset:offset + order]
        if chunk not in freq:
            freq[chunk] = 1
        else:
            freq[chunk] += 1
        offset += 1

        if offset + order >= len(text):
            done = True

    return freq



def generate(frequency: dict[str], seed: str, length_to_generate: int = 0) -> str:
    order: int = len(list(frequency.keys())[0])
    gen: list[str] = list()

    if order != len(seed) + 1 or order < 1:
        raise ValueError

    current_letters: str = copy.deepcopy(seed)

    for i in range(0, length_to_generate):
        total_frequency: int = sum(
                [frequency[current_letters + ALPHABET[i]] for i in range(0, len(ALPHABET))
                    if current_letters + ALPHABET[i] in frequency
                ]
        )

        # Nothing more to generate: skip.
        if total_frequency == 0:
            continue

        n: int = random.randint(0, total_frequency)

        # See also:
        # https://stackoverflow.com/a/14992648

        done: bool = False
        j: int = 0
        value: int = 0

        while not done and j < len(ALPHABET):
            # Retrieve older list if n did not pass the test.
            cm = copy.deepcopy(current_letters)

            # Add the current element to the indices.
            cm = cm + ALPHABET[j]

            if cm in freq:

                n -= frequency[cm]

                if n <= 0:
                    value = j
                    done = True
                    # Remove the head of the list because it's an element
                    # older than the order we're considering
                    cm = cm[1:]
                    current_letters = cm

                j += 1

            else:
                cm = cm[:-1]
                j += 1

        gen.append(ALPHABET[value])

    return ''.join(gen)


def select_seed(text: str, order: int) -> str:
    r"""Select a random seed of length order-1."""
    i: int = random.randint(0, len(text) - order - 1)
    return text[i:i + order - 1]


if __name__ == '__main__':
    if len(sys.argv) < 4:
        print(sys.argv[0] + ' order length iterations')
        sys.exit(1)

    order: int = int(sys.argv[1])
    length: int = int(sys.argv[2])
    iterations: int = int(sys.argv[3])

    if order < 1:
        print('order too small')
        sys.exit(1)

    print('order: 1->' + str(order))
    print('length: ' + str(length))
    print('iterations: ' + str(iterations))

    with open('text_urls.txt', 'r') as f:
        urls: list[str] = f.readlines()

    text: io.BytesIO = b''
    for u in urls:
        with urllib.request.urlopen(u) as response:
            text += response.read()

    soup = BeautifulSoup(text, 'html.parser')
    text = soup.get_text(' ', strip=True)

    text = normalize_text(text)

    with open('generated.txt', 'w') as f:
        f.write('==normalized text:\n')
        f.write(text)
        f.write('\n\n')
        for order_tmp in range(1, order + 1):
            freq = classifier(text, order_tmp)
            for i in range(0, iterations):
                seed = select_seed(text, order_tmp)

                print('order: ' + str(order_tmp) + '; current iteration: ' + str(i))
                generated: str = generate(freq, seed[:order_tmp - 1], length)

                f.write('==order: ' + str(order_tmp) + '; seed: "' + seed + '"; iteration ' + str(i) + '==\n')
                f.write(generated)
                f.write('\n\n')
