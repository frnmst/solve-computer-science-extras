# Instructions

1. install

   ```shell
   make install-dev
   ```

2. run

   ```shell
   make run
   ```

3. edit the `text_urls.txt` file to change sources
