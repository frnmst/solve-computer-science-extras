#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2023 Franco Masotti
#
# SPDX-License-Identifier: MIT

class ProgramError(Exception):
    r"""Program generated an error."""


class CoreMemory(list):
    def __init__(self, size: int):
        # Initialize list with undefined elements.
        super().__init__([None for i in range(0, size)])

    def __getitem__(self, key: int):
        # Circular list using mod operator.
        return super().__getitem__(key % len(self))

    def __setitem__(self, key: int, value: int):
        # Circular list using mod operator.
        return super().__setitem__(key % len(self), value)


class Simulator:
    def __init__(self, size: int, pointer: int = 0):
        self.instruction_pointer = pointer
        self.core = CoreMemory(size)

    def _decode_addressing(self, addressing_type: str) -> str:
        if addressing_type == '0':
            addressing = 'immediate'
        elif addressing_type == '1':
            addressing = 'direct'
        elif addressing_type == '2':
            addressing = 'indirect'
        else:
            addressing = str()

        return addressing

    def run(self):
        machine_code: str = self.core[self.instruction_pointer]
        m: tuple = self.split_machine_code(machine_code)

        op = m[0]
        addressing_a = self._decode_addressing(m[1])
        sign = m[2]

        if sign == '0':
            A = int(m[3])
        else:
            A = - int(m[3])

        addressing_b = self._decode_addressing(m[4])
        if addressing_b == str():
            B = str()
        else:
            sign = m[5]

            if sign == '0':
                B = int(m[6])
            else:
                B = - int(m[6])

        self.execute_operation(op, A, addressing_a, B, addressing_b)

    def store_operation(self, machine_code: str):
        # Store the operation in memory.
        if machine_code is None or len(machine_code) != 15:
            raise ProgramError

        self.core[self.instruction_pointer] = machine_code


    def execute_operation(self, op: str, A: int, addressing_a: str, B: int, addressing_b: str):
        # Execute the instructions.
        if op == '1':
            self._MOV(A, addressing_a, B, addressing_b)
        elif op == '0':
            self._DAT(A, addressing_a)


    def human_mnemonics_2_machine_code(self, arg: str, code_only: bool = True) -> tuple:
        # This method prints and returns the machine code and other values
        #
        # 'str+3 [#,@,]<int>[ [#,@,]<int>]'
        # instructions
        #
        # MOV 1
        # ADD 2
        # SUB 3
        # JMP 4
        # JMZ 5
        # JMG 6
        # DJZ 7
        # CMP 8
        # PRN 9
        # DAT 0
        #

        # Keep track of current index.
        ptr = 0
        if arg[ptr: ptr + 3] == 'MOV':
            op = '1'
        elif arg[ptr: ptr + 3] == 'DAT':
            op = '0'

        ptr += 3

        # space
        ptr += 1

        addressing_a = arg[ptr:ptr + 1]
        data_offset = 0
        if addressing_a == '#':
            type_a = '0'
        elif addressing_a == '@':
            type_a = '2'
        else:
            type_a = '1'
            data_offset = -1

        ptr += 1 + data_offset

        data_a = arg[ptr + data_offset:ptr + 5 + data_offset].split()[0]
        ptr += len(data_a)

        int(data_a)
        if int(data_a) >= 0:
            sign_a = '0'
        else:
            sign_a = '1'

        # Remove sign and simulate overflow if input number is greater than
        # the maximum supported.
        data_a = str(abs(int(data_a)) % (10 ** 5))

        # Skip space
        ptr += 1

        addressing_b = arg[ptr:ptr + 1]
        data_offset = 0
        if addressing_b == '#':
            type_b = '0'
        elif addressing_b == '@':
            type_b = '2'
        elif addressing_b == str():
            # DAT instruction.
            type_b = 'F'
        else:
            type_b = '1'
            data_offset = -1

        ptr += 1

        # DAT instruction.
        sign_b: str = 'F'
        data_b: str = 'F' * 5

        # If not DAT instruction.
        if type_b != 'F':
            data_b = arg[ptr + data_offset:ptr + 5 + data_offset].split()[0]
            ptr += len(data_b)

            # All cases except the DAT instruction.
            sign_b = '0'
            if len(data_b) > 0:
                int(data_b)
                if int(data_b) >= 0:
                    sign_b = '0'
                else:
                    sign_b = '1'

                # Remove sign and simulate overflow if input number is greater than
                # the maximum supported.
                data_b = str(abs(int(data_b)) % (10 ** 5))

        machine_code = op + type_a + sign_a + data_a.zfill(5) + type_b + sign_b + data_b.zfill(5)
        # print('exec: ' + str(self.instruction_pointer).zfill(4) + ' = ' + machine_code)

        # Restore signs and transform to int.
        if sign_a == 1:
            A = -int(data_a)
        else:
            A = int(data_a)

        if data_b == 'F' * 5:
            B = str()
        else:
            if sign_b == 1:
                B = -int(data_b)
            else:
                B = int(data_b)

        addressing_a = self._decode_addressing(type_a)
        addressing_b = self._decode_addressing(type_b)

        if code_only:
            return machine_code
        else:
            return machine_code, op, A, addressing_a, B, addressing_b

    def machine_code_2_human_mnemonics(self, machine_code_fields: tuple) -> tuple:
        # machine_code_fields = op, type_a, sign_a, dat_a, type_b, sign_b, dat_b

        if machine_code_fields[0][0] == '1':
            instruction = 'MOV'
        elif machine_code_fields[0][0] == '0':
            instruction = 'DAT'

        if machine_code_fields[1][0] == '0':
            addressing_a = 'immediate'
        elif machine_code_fields[1][0] == '1':
            addressing_a = 'direct'
        elif machine_code_fields[1][0] == '2':
            addressing_a = 'indirect'

        if machine_code_fields[2][0] == '0':
            sign_a = '+'
        elif machine_code_fields[2][0] == '1':
            sign_a = '-'

        dat_a = str(int(machine_code_fields[3]))

        # Special case for the DAT instruction.
        if len(machine_code_fields[4]) == 0:
            addressing_b = sign_b = dat_b = str()
        else:
            if machine_code_fields[4][0] == '0':
                addressing_b = 'immediate'
            elif machine_code_fields[4][0] == '1':
                addressing_b = 'direct'
            elif machine_code_fields[4][0] == '2':
                addressing_b = 'indirect'

            if machine_code_fields[5][0] == '0':
                sign_b = '+'
            elif machine_code_fields[5][0] == '1':
                sign_b = '-'

            dat_b = str(int(machine_code_fields[6]))


        return instruction, addressing_a, sign_a, dat_a, addressing_b, sign_b, dat_b

    def split_machine_code(self, arg: str) -> tuple:
        if arg is None or len(arg) != 15:
            raise ProgramError

        op = arg[0]
        int(op)

        type_a = arg[1]
        int(type_a)

        sign_a = arg[2]
        int(sign_a)

        dat_a = arg[3:8]
        int(dat_a)

        # Special case for DAT instruction.
        if arg[8] == 'F':
             type_b = ''
             sign_b = ''
             dat_b = ''
        else:
            type_b = arg[8]
            int(type_b)

            sign_b = arg[9]
            int(sign_b)

            dat_b = arg[10:16]
            int(dat_b)

        return op, type_a, sign_a, dat_a, type_b, sign_b, dat_b

    def _MOV(self, A: str, addressing_a: str, B: str, addressing_b: str) -> str:
        if addressing_a == 'immediate':
            # Store.
            pointed_a, _op, _A, _addressing_a, _B, _addressing_b = self.human_mnemonics_2_machine_code('DAT ' + str(A), False)
            self.execute_operation(_op, _A, _addressing_a, _B, _addressing_b)

        elif addressing_a == 'direct':
            pointed_a = self.core[self.instruction_pointer + A]

        elif addressing_a == 'indirect':
            # + sign
            d = self.split_machine_code(self.core[self.instruction_pointer + A])
            # Temporary instruction pointer to do indirect reference.
            ip = self.instruction_pointer + A
            if d[2] == '0':
                move_0 = int(d[3])
            elif d[2] == '1':
                move_0 = - int(d[3])

            pointed_a = self.core[ip + move_0]

        if addressing_b == 'immediate':
            # Get integer part of B.
            #d = self.human_mnemonics_2_machine_code('DAT ' + str(B))
            d, _op, _A, _addressing_a, _B, _addressing_b = self.human_mnemonics_2_machine_code('DAT ' + str(B), False)
            self.execute_operation(_op, _A, _addressing_a, _B, _addressing_b)

            if d[2] == 0:
                pointed_b = int(d[3])
            elif d[2] == 1:
                pointed_b = - int(d[3])
        elif addressing_b == 'direct':
            pointed_b = B
        elif addressing_b == 'indirect':
            pass

        self.core[self.instruction_pointer + pointed_b] = pointed_a

        # Go to the next instruction.
        self.instruction_pointer += 1

    def _ADD(self, A: str, B: str):
        pass

    def _SUB(self, A: str, B: str):
        pass

    def _JMP(self, A: str, B: str):
        pass

    def _JMZ(self, A: str, B: str):
        pass

    def _DJZ(self, A: str, B: str):
        pass

    def _CMP(self, A: str, B: str):
        pass

    def _DAT(self, B: str, addressing_b: str):
        r"""DAT is not meant to be an executable instruction."""
        pass

    def _PRN(self):
        try:
            print('addr\t= ' + str(self.instruction_pointer))
            print('human\t= ' + ' '.join(self.machine_code_2_human_mnemonics(self.split_machine_code(self.core[self.instruction_pointer]))))
            print('raw\t= ' + str(self.core[self.instruction_pointer]))
        except ProgramError:
            pass

    def _dump(self):
        # Warning:
        # This method alters the instruction pointer!
        self.instruction_pointer = 0
        for i in range(0, len(self.core)):
            self._PRN()
            print("----------")
            self.instruction_pointer += 1


if __name__ == '__main__':
    # Machine string
    #
    # fields: [instruction, addressing_a, sign_a, data_a, addressing_b, sign_b, data_b]
    # index:      0             1           2      3:7         8          9     10:14
    #
    # The 'DAT' instruction has all 'F's in the 'B' part (indices from 8 to 14)
    # Examples
    #
    # DAT 1     ==           '01000001FFFFFFF'
    # DAT -1    ==           '01100001FFFFFFF'

    # Immediate addressing.
    # exec this:    mem[ip + 10] = 1
    # MOV #1 10  ==          '100000011000010'

    # Direct addressing.
    # exec this:    mem[ip + 10] = mem[ip + 1]
    # MOV 1 10  ==           '110000011000010'

    # Indirect addressing.
    # exec this:    mem[ip -10] = mem[mem[ip + 1]]
    # MOV @1 -10  ==         '120000011100010'

    print('\ncore m:')
    m = Simulator(600, 413)

    m.instruction_pointer = 413
    m.store_operation(m.human_mnemonics_2_machine_code('DAT 22'))

    m.instruction_pointer = 415
    m.store_operation(m.human_mnemonics_2_machine_code('MOV @3 100'))

    m.instruction_pointer = 418
    m.store_operation(m.human_mnemonics_2_machine_code('DAT -5'))

    # Reset instruction pointer.
    m.instruction_pointer = 415

    # Run the simulation.
    m.run()

    m._dump()

    # DAT 22 in absolute position 515.
    assert m.core[515] == '01000022FFFFFFF'
    assert m.core[515] == m.human_mnemonics_2_machine_code('DAT 22')

    # DAT 22
    # 01000022FFFFFFF
    # m.core[413] = '0' + '1' + '0' + '22'.zfill(5) + 'F' * 7

    # MOV @3 100
    # 120000031000100
    # m.core[415] = '1' + '2' + '0' + '3'.zfill(5) + '1' + '0' + '100'.zfill(5)

    # DAT -5
    # 01100005FFFFFFF
    # m.core[418] = '0' + '1' + '1' + '5'.zfill(5) + 'F' * 7

    print('\ncore n:')
    n = Simulator(10, 0)

    n.store_operation(n.human_mnemonics_2_machine_code('MOV 0 1'))

    # MOV 0 1
    # 110000001100001
    # n.core[0] = '1' + '1' + '0' + '0'.zfill(5) + '1' + '1' + '1'.zfill(5)

    n._dump()
    n.instruction_pointer = 0

    print("==============")

    for i in range(0,100000):
        n.run()

    n._dump()
