#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2023 Franco Masotti
#
# SPDX-License-Identifier: MIT

import copy
import io
import random
import string
import sys
import urllib.request

import numpy as np
from bs4 import BeautifulSoup

ALPHABET = list()
for i in string.ascii_uppercase:
    ALPHABET.append(i)
ALPHABET.append("'")
ALPHABET.append(' ')


def normalize_text(text: str) -> str:
    final_text: list = list()

    i: int = 0
    while i < len(text):
        j: int = i
        while j < len(text) and text[j] in string.whitespace:
            j += 1

        if j < len(text) and j > i:
            final_text.append(' ')

        # Skip sequential whitespaces.
        i = j

        if j < len(text):
            if text[i].upper() in ALPHABET:
                final_text.append(text[i].upper())

        i += 1

    return ''.join(final_text)



def gen_matrix(dimensions: int = 1, side: int = 1) -> np.ndarray:
    if dimensions < 1 or side < 1:
        raise ValueError

    shape: tuple = tuple()
    for i in range(1, dimensions + 1):
        shape += (side, )
    matrix = np.zeros(shape, dtype='uint16')

    return matrix


def classifier(text: str, order: int = 1) -> list:
    if order < 1:
        raise ValueError

    # Generate a matrix with the correct side.
    matrix_side: int = len(ALPHABET)
    freq = gen_matrix(order, matrix_side)

    if len(text) > order - 1:
        # Start index
        i = order - 1

        while i < len(text):
            # Find correct matrix line of the previous character.
            prev_alpha_idx: list = list()
            valid_idx: list = list()

            # Start from the end of the section.
            j = i

            # len(valid_idx)
            k: int = 0
            while j >= 0 and k < order:
                if text[j] in ALPHABET:
                    valid_idx.append(j)
                    k += 1

                j -= 1

            # Jump to the next character.
            i += 1
            while i < len(text) and text[i] not in ALPHABET:
                i += 1

            # valid_alpha_idx[0:-1] == prevouis chars
            # valid_alpha_idx[-1] == current index (i)
            # Index to alphabet index:
            for j in range(0, len(valid_idx) - 1):
                prev_alpha_idx.append(ALPHABET.index(text[valid_idx[j]]))

            idx: tuple = tuple(prev_alpha_idx)

            if len(valid_idx) > 0:
                idx += (ALPHABET.index(text[valid_idx[-1]]), )

                # Reverse the indices because we start the analysis from the
                # end of the substring.
                idx = idx[::-1]

                freq[idx] += 1

    return freq


def generate_seed(start_string: str) -> list:
    seed: list = list()
    for c in start_string:
        if c.isalpha():
            seed.append(ord(c.upper()) - 65)
        elif c == "'":
            seed.append(26)
        elif c == ' ':
            seed.append(27)
        else:
            seed.append(0)

    return seed



def generate(frequency: np.ndarray, seed: list, length_to_generate: int = 0):
    r"""Order is deduced by matrix ndim."""
    order: int = frequency.ndim
    result: str = str()

    assert order == len(seed) + 1

    current_letters = seed
    for c in current_letters:
        print(ALPHABET[c].lower(), end='', flush=True)

    for i in range(0, length_to_generate):
        total_frequencies: int = np.sum(frequency[tuple(current_letters)])
        if total_frequencies == 0:
            print("-----err-----")
            sys.exit(1)

        n: int = random.randint(1, total_frequencies)

        # https://stackoverflow.com/a/14992648
        done: bool = False
        j: int = 0
        value: int = 0

        while not done and j < len(ALPHABET):
            # Retrieve older list if n did not pass the test.
            cm = copy.deepcopy(current_letters)

            # Add the current element to the indices.
            cm.append(j)

            n -= frequency[tuple(cm)]

            if n <= 0:
                value = j
                done = True
                # Remove the head of the list because it's an element
                # older than the order we're considering
                cm.pop(0)
                current_letters = cm

            j += 1

        print(ALPHABET[value].lower(), end='', flush=True)
    print()


def select_seed(text: str, order: int) -> str:
    i = random.randint(0, len(text) - order - 1)
    return text[i:i + order - 1]


if __name__ == '__main__':
    if len(sys.argv) < 4:
        print(sys.argv[0] + ' order length url')
        sys.exit(1)

    order: int = int(sys.argv[1])
    length: int = int(sys.argv[2])
    url: str = sys.argv[3]

    print('order: ' + str(order))
    print('length: ' + str(length))

    with urllib.request.urlopen(url) as response:
        text: io.BytesIO = response.read()

    soup = BeautifulSoup(text, 'html.parser')
    text = soup.get_text()

    text = normalize_text(text)

    seed = select_seed(text, order)
    print('seed: ' + repr(seed))
    freq = classifier(text, order)

    print('---')
    generate(freq, generate_seed(seed[:order - 1]), length)
    print('---')
