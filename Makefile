export PACKAGE_NAME=solve_comuter_science_extras

# See
# https://docs.python.org/3/library/venv.html#how-venvs-work
export VENV_CMD=. .venv/bin/activate

default: install-dev

install-dev:
	python3 -m venv .venv
	$(VENV_CMD) \
		&& pip install --requirement requirements-freeze.txt \
		&& deactivate
	$(VENV_CMD) \
		&& pre-commit install \
		&& deactivate
	$(VENV_CMD) \
		&& pre-commit install --hook-type commit-msg \
		&& deactivate

regenerate-freeze: uninstall-dev
	python3 -m venv .venv
	$(VENV_CMD) \
		&& pip install --requirement requirements.txt --requirement requirements-dev.txt \
		&& pip freeze --local > requirements-freeze.txt \
		&& deactivate

uninstall-dev:
	rm -rf .venv

update: install-dev
	$(VENV_CMD) \
		&& pre-commit autoupdate \
			--repo https://github.com/pre-commit/pre-commit-hooks \
			--repo https://github.com/pycqa/isort \
			--repo https://codeberg.org/frnmst/md-toc \
			--repo https://github.com/jorisroovers/gitlint \
		&& deactivate

pre-commit:
	$(VENV_CMD) \
		&& pre-commit run --all \
		&& deactivate

clean:
	rm -rf build dist *.egg-info tests/benchmark-results
	# Remove all markdown files except the readmes.
	find -regex ".*\.[mM][dD]" ! -name 'README.md' ! -name 'CONTRIBUTING.md' -type f -exec rm -f {} +
	$(VENV_CMD) \
		&& $(MAKE) -C docs clean \
		&& deactivate

.PHONY: default install-dev regenerate-freeze uninstall-dev update clean pre-commit
