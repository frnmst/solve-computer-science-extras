#!/usr/bin/env python3

import hashlib

class Vertex:
    def __init__(self, name: str, ident: int):
        self.name: str = name
        self.ident: int = ident
        
    def print_vertex(self):
        print('"' + self.name + '", ', end = ' ')
        print('id = ' + str(self.ident))

class Edge:
    def __init__(self, vertex_start: int, vertex_end: int, weight: int):
        self.vertex_start: int = vertex_start
        self.vertex_end: int = vertex_end
        self.weight: int = weight


class AdjElementTreeNode:
    def __init__(self, edge_data: list):
        self.value: list = edge_data
        self.left = None
        self.right = None
        # Absolute root of the tree.
        self.root = None

    def insert(self, root, edge_data: list, counter: int = 0):
        if root is None:
            node = AdjElementTreeNode(edge_data)
            # Set the absolute root of the tree, not the current root.
            if counter == 0:
                self.root = node
            return node
        else:
            counter += 1
            if edge_data[0] > root.value[0]:
                root.right = self.insert(root.right, edge_data, counter)
            elif edge_data[0] < root.value[0]:
                root.left = self.insert(root.left, edge_data, counter)

            return root

    # Print in-order.
    def print_tree(self, root):
        if root is None:
            return

        self.print_tree(root.left)
 
        print(root.value, end=' ')
 
        self.print_tree(root.right)


    """
    def search(self, edge_data: list):
        if root is not None:
            if edge_data[0] == self.root['value']:
                return root
            else:
                if edge_data[0] > self.root['value'][0]:
                    self.search(self.root['right'], edge_data)
                else:
                    self.search(self.root['left'], edge_data)
            else:
                # Not found.
                return None
    """

class Graph:
    def __init__(self):
        self.adj: dict = dict()
        self.vertices: list = list()

    def print_graph(self):
        print('adj list')
        for v_ident_start in self.adj:
            print(v_ident_start, end=': ')
            AdjElementTreeNode.print_tree(self.adj[v_ident_start], self.adj[v_ident_start].root)
            print()
        print()
        print('existing vertices')
        for v in self.vertices:
            v.print_vertex()

    def add_vertex(self, v: Vertex):
        # v.ident must be unique
        if v.ident not in self.adj:
            # O(1): see
            # https://stackoverflow.com/questions/17539367/python-dictionary-keys-in-complexity
#            self.adj[v.ident]: list = list()
            self.adj[v.ident] = AdjElementTreeNode(None)
            self.vertices.append(v)

    def add_edge(self, e: Edge):
#        self.adj[e.vertex_start.ident].append([e.vertex_end.ident, e.weight])
        self.adj[e.vertex_start.ident].insert(self.adj[e.vertex_start.ident].root, [e.vertex_end.ident, e.weight])


    def remove_vertex(self, v: Vertex):
        pass

    def remove_edge(self, e: Edge):
        pass


if __name__ == '__main__':
    g = Graph()
    # TODO: use real european city names and distances in Meters, then use
    # Dijstra.
    # "Toy SatNav"
    v1 = Vertex(name='one', ident=1)
    v2 = Vertex(name='two', ident=2)
    v3 = Vertex(name='three', ident=3)
    v4 = Vertex(name='four', ident=4)
    v5 = Vertex(name='five', ident=5)
    g.add_vertex(v1)
    g.add_vertex(v2)
    g.add_vertex(v3)
    g.add_vertex(v4)
    g.add_vertex(v5)
    e0 = Edge(v1, v2, 30)
    e1 = Edge(v2, v3, 5)
    e2 = Edge(v2, v5, 20)
    e3 = Edge(v2, v4, 10)
    e4 = Edge(v3, v4, 15)
    e5 = Edge(v4, v2, 10)
    e6 = Edge(v5, v3, 15)
    e7 = Edge(v5, v4, 10)

    g.add_edge(e0)
    g.add_edge(e1)
    g.add_edge(e2)
    g.add_edge(e3)
    g.add_edge(e4)
    g.add_edge(e5)
    g.add_edge(e6)
    g.add_edge(e7)

    g.print_graph()
