# Instructions

Set up Whoogle with the
[docker-compose file](https://github.com/benbusby/whoogle-search/blob/main/docker-compose.yml).

If you use Apache as HTTP webserver, import the
[provided configuration file](./whoogle.conf) and edit it based on your needs.
